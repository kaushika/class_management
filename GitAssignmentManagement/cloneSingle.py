#!/usr/bin/python
# 
import  sys, os, subprocess, argparse
from progress.bar import Bar
from subprocess import check_output
from subprocess import call
from subprocess import Popen

#DEF CONSTANTS
GIT_BASE = "git@gitlab.socs.uoguelph.ca:2430F19/"
ASSIGNMENT_REPO = "a2.git"
STDNT_WORK_DIR = "singles"

def makeChangeDir(dirname):
	if not os.path.isdir(dirname):
		call(["mkdir", dirname])
	os.chdir(dirname)

def changeWorkingDirectory(targetDir):
	current = os.getcwd()
	current = current.rstrip('\n')
	newDir = "/".join([current,targetDir])
	makeChangeDir(newDir)
	return current

def cloneSingle(username, gitLabBase, assignmentRepo):
	#print(student)#change to progress
	makeChangeDir(username)
	gitrepo = '/'.join([gitLabBase,username,assignmentRepo])# you made need to add delimiters
	p = Popen(['git','clone', gitrepo ],stdout=subprocess.PIPE, stderr=subprocess.STDOUT) #,shell=True,stderror=subprocess.STDOUT,)
	good, output = p.communicate()


def main(args):
	#COMMAND LINE ARGS
	parser = argparse.ArgumentParser()
	parser.add_argument("-b" , "--gitbase", type=str, help="Git base", default=GIT_BASE)
	parser.add_argument("-a" , "--assignmentRepo" , type=str, help="Assignment Repository", default=ASSIGNMENT_REPO)
	parser.add_argument("-d" , "--studentWorkDirectory", type=str, help="Directory for student's work", default=STDNT_WORK_DIR)
	args = parser.parse_args()
	
	#MAIN
	home = os.getcwd();
	currentDir = changeWorkingDirectory(args.studentWorkDirectory)#changing/adding parent working directory
	username = input("Username of student to clone:")
	cloneSingle(username, args.gitbase, args.assignmentRepo)
	os.chdir(home)
	return 

if __name__ == '__main__':
	import sys
	sys.exit(main(sys.argv))
