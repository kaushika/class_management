#!/usr/bin/python
# 
import  sys, os, subprocess, argparse
from progress.bar import Bar
from subprocess import check_output
from subprocess import call
from subprocess import Popen

#DEF CONSTANTS
GIT_BASE = "git@gitlab.socs.uoguelph.ca:2430F19"
ASSIGNMENT_REPO = "A1.git"
STDNT_WORK_DIR = "coursework"
CLASS_LIST = "classlist.txt"

def makeChangeDir(dirname):
	if not os.path.isdir(dirname):
		call(["mkdir", dirname])
	os.chdir(dirname)

def changeWorkingDirectory(targetDir):
	current = os.getcwd()
	current = current.rstrip('\n')
	newDir = "/".join([current,targetDir])
	makeChangeDir(newDir)
	return current

def cloneFromList(inFile, gitLabBase, assignmentRepo):
	workingDir = os.getcwd()
	fp = open(inFile, 'r')
	count = len(fp.readlines())
	fp.close()
	with open(inFile, "r") as classList:
		bar = Bar('Processing', max=count)
		for student in classList:
			student = student.rstrip('\n');
			#print(student)#change to progress
			makeChangeDir(student)
			gitrepo = '/'.join([gitLabBase,student,assignmentRepo])# you made need to add delimiters
			#print(gitrepo)
			p = Popen(['git','clone', gitrepo ],stdout=subprocess.PIPE, stderr=subprocess.STDOUT) #,shell=True,stderror=subprocess.STDOUT,)
			good, errors = p.communicate()
			#print(good)
			bar.next()
			os.chdir(workingDir)
		bar.finish()

def main(args):
	#COMMAND LINE ARGS
	parser = argparse.ArgumentParser()
	parser.add_argument("-b" , "--gitbase", type=str, help="Git base", default=GIT_BASE)
	parser.add_argument("-a" , "--assignmentRepo" , type=str, help="Assignment Repository", default=ASSIGNMENT_REPO)
	parser.add_argument("-d" , "--studentWorkDirectory", type=str, help="Directory for student's work", default=STDNT_WORK_DIR)
	parser.add_argument("-c" , "--classList", type=str, help="list of usernames in .txt, .csv or no extension", default=CLASS_LIST)
	args = parser.parse_args()
	
	#MAIN
	home = os.getcwd();
	listFile = "/".join([home,args.classList])
	currentDir = changeWorkingDirectory(args.studentWorkDirectory)#changing/adding parent working directory
	print("Creating student folders in: " +currentDir)
	cloneFromList(listFile, args.gitbase, args.assignmentRepo)
	return 

if __name__ == '__main__':
	import sys
	sys.exit(main(sys.argv))
