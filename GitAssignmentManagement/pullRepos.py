#!/usr/bin/python
# 
import  csv,sys, os, re, subprocess, argparse
from subprocess import check_output
from subprocess import Popen
from progress.bar import Bar

#DEF CONSTANTS
GIT_BASE = "git@gitlab.socs.uoguelph.ca:2430F19"
ASSIGNMENT_REPO = "A1"
STDNT_WORK_DIR = "coursework"


def pullRepos(target, repo):
	count = len(next(os.walk(target))[1])
	home = "/".join([os.getcwd(),target])
	os.chdir(target) #target dir must exist
	bar = Bar('Processing', max=count)
	for student in os.listdir('.'):
		#print "working on " + student
		studentDir = "/".join([student,repo])
		#print(studentDir)
		if (os.path.isdir(studentDir)):
			os.chdir(studentDir)
			p = Popen(["git", "pull"  ],stdout=subprocess.PIPE, stderr=subprocess.STDOUT) #,shell=True,stderror=subprocess.STDOUT,)
			good, text = p.communicate()
			bar.next()
			os.chdir(home)
	bar.finish()

def main(args):
	#COMMAND LINE ARGS
	parser = argparse.ArgumentParser()
	parser.add_argument("-a" , "--assignmentRepo" , type=str, help="Repo Name (just the directory)", default=ASSIGNMENT_REPO)
	parser.add_argument("-d" , "--studentWorkDirectory", type=str, help="Directory holding student repositories", default=STDNT_WORK_DIR)
	args = parser.parse_args()
	
	#MAIN
	home = os.getcwd();
	pullRepos( args.studentWorkDirectory, args.assignmentRepo)
	return 

if __name__ == '__main__':
	import sys
	sys.exit(main(sys.argv))
		
