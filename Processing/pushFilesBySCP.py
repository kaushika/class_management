#!/usr/bin/python
# 
import  csv,sys, os, re, pexpect, getpass
from subprocess import call
from subprocess import check_output
from shutil import rmtree
password = getpass.getpass("password for exam acounts: ")
home = os.getcwd()

accounts = []
for i in range(0,16):
	accounts.append("cis2430_"+str(i))


for acc in accounts:
	child = pexpect.spawn('scp bin/setupCE '+ acc+'@linux:bin/')
	child.expect(acc+"@linux's password")
	child.sendline(password)
	child.expect(pexpect.EOF, timeout=None)

	child = pexpect.spawn('scp Comp_Exam1_Questions.gz '+ acc+'@linux:')
	child.expect(acc+"@linux's password")
	child.sendline(password)
	child.expect(pexpect.EOF, timeout=None)


		