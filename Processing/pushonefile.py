#!/usr/bin/python
# 
import  csv,sys, os, re, pexpect, getpass
from pexpect import pxssh
import getpass
import random



def pushFile(f, username, password):
	command = 'scp '+ f+" "+ username+'@linux.socs.uoguelph.ca:Desktop/'
	expected = username+"@linux.socs.uoguelph.ca's password:"
	child = pexpect.spawn(command)
	child.expect(expected)
	child.sendline(password)
	child.expect(pexpect.EOF, timeout=None)


def makeExecutable(username, password):
	s = pxssh.pxssh()
	try:
		s.login('linux.socs.uoguelph.ca', username, password)
		s.sendline('cd ~/Desktop')   # run a command
		s.prompt()             # match the prompt
		print(s.before)        # print everything before the prompt.
		s.sendline('chmod 755 CETimer.py')   # run a command
		s.prompt()             # match the prompt
		print(s.before)        # print everything before the prompt
		s.logout()
	except pxssh.ExceptionPxssh as e:
		print("pxssh failed on login.")
		print(e)
		
		
hostname = "linux.socs.uoguelph.ca"
password = getpass.getpass("password for exam acounts: ")

accounts = []
for i in range(0,17):
	accounts.append("cis2430_"+str(i))

filename = "CETimer.py"

for acc in accounts:
	print("working on "+ acc)
	pushFile(filename,acc,password)
	print("changing permissions")
	makeExecutable(acc, password)
	
	
