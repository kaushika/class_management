import pexpect
from pexpect import pxssh
import getpass
import random

def cleanup(username, hostname, password):
	s = pxssh.pxssh()
	try:
		s.login(hostname, username, password)
		s.sendline('rm -rf ~/Desktop/test')   # run a command
		s.prompt()             # match the prompt
		print(s.before)        # print everything before the prompt.
		s.sendline('mkdir ~/Desktop/test')   # run a command
		s.prompt()             # match the prompt
		print(s.before)
		#s.sendline('killall python')   # run a command
		#s.prompt()             # match the prompt
		#print(s.before) 
		s.logout()
	except pxssh.ExceptionPxssh as e:
		print("pxssh failed on login.")
		print(e)


def pushExam(test, password):
	command = 'scp -r test/'+test+" "+ username+'@linux.socs.uoguelph.ca:Desktop/test/'
	expected = username+"@linux.socs.uoguelph.ca's password:"
	child = pexpect.spawn(command)
	child.expect(expected)
	child.sendline(password)
	child.expect(pexpect.EOF, timeout=None)

def selectExam():
	testNum = random.randint(1,22)
	
	return "T" + str(testNum)


		
hostname = "linux.socs.uoguelph.ca"
num = raw_input('Which account? (Just the number- enter 99 to quit):')
password = getpass.getpass('password: ')

while num != "99":

	username = "cis2430_"+ num
	cleanup(username, hostname, password)
	pushExam(selectExam(), password)
	num = raw_input('Which account? (Just the number- enter 99 to quit):')
