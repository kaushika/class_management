# class_management

A collection of scripts that can be used for managing assignment submissions for large classes

## Assumptions

* All git-related scripts assume that a public key has been set up for ssh access to the git server
* Most scripts make use of the argparse package and will respond to a -h flag with a list of possible command line arguments

All of these scripts are specialized purpose and need refactoring.

Most are in python
