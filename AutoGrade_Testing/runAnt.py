#!/usr/bin/python
# 
import  csv,sys, os, re, subprocess, shutil
from subprocess import check_output, call
from subprocess import Popen

def removePackage(filename):
	with open(filename, "r") as f:
		lines = f.readlines()
	with open(filename, "w") as f:
		for line in lines:
			if not "package" in line.strip("\n"):
				f.write(line)

def getfiles(path):
	if os.path.isdir(path):
		for root, dirs, files in os.walk(path):
			if ".git" in dirs:
				dirs.remove(".git")
			if ".svn" in dirs:
				dirs.remove(".svn")
			if ".empty" in files:
				files.remove(".empty")
			if(".git" in files):
				files.remove(".git")
			if(".DS_Store" in files):
				files.remove(".DS_Store")
			for name in files:
				yield os.path.join(root, name)
	else:
		yield path

workDir = raw_input("target directory? ")
workDir = workDir.rstrip('\n')
homeDir = check_output("pwd")
homeDir = homeDir.rstrip('\n')
targetDir = homeDir+'/'+workDir
# filename = raw_input("Class List filename? ")
# classList = open (filename)

os.chdir(workDir)
for student in os.listdir("."):
	if(".DS_Store" in student):
		continue
	studentDir = student+'/a2/'
	#print student
	#print pathname
	if (os.path.isdir(studentDir)):
		#print  student
		os.chdir(studentDir)
		p = Popen( ["ant" ],stdout=subprocess.PIPE, stderr=subprocess.STDOUT) #,shell=True,stderror=subprocess.STDOUT,)
		good, text = p.communicate()
		#print(good)
		if("BUILD SUCCESSFUL" in good):
			print student

	# for line in good.split("\n"):
	# 	if "passed:" in line:
	# 		pos = line.rfind(":")
	# 		num = line[pos+1:]
	# 		print student + ","+ num
	#print(text)
	os.chdir(targetDir)

		
