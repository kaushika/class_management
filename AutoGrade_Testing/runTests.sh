#!/bin/bash
MINPARAMS=2
if [  $# -lt $MINPARAMS ]
then 
    echo
    echo "You must provide a  FILENAME for the class list and a FILENAME for the summary csv file"
    exit 0
fi




for user in `cat $1`; do
dir = $user/a3/
cp myMake dir
cp *.c dir
cp *.h dir
cd dir
make -f myMake > makeError.txt 2>&1
cat makeError.txt | mail -s "results from make" judi@uoguleph.ca
./testRun
cat 'filename' | mail -s "results from testing your list" judi@uoguelph.ca
cd ../../
done
