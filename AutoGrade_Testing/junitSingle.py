#!/usr/bin/python
# 
import  csv,sys, os, re, subprocess, shutil
from subprocess import check_output, call
from subprocess import Popen

def removePackage(filename):
	with open(filename, "r") as f:
		lines = f.readlines()
	with open(filename, "w") as f:
		for line in lines:
			if not "package" in line.strip("\n"):
				f.write(line)

def getfiles(path):
	if os.path.isdir(path):
		for root, dirs, files in os.walk(path):
			if ".git" in dirs:
				dirs.remove(".git")
			if ".svn" in dirs:
				dirs.remove(".svn")
			if ".empty" in files:
				files.remove(".empty")
			if(".git" in files):
				files.remove(".git")
			if(".DS_Store" in files):
				files.remove(".DS_Store")
			for name in files:
				yield os.path.join(root, name)
	else:
		yield path


homeDir = check_output("pwd")
homeDir = homeDir.rstrip('\n')

shutil.rmtree("src")
call(["mkdir", "src"])
os.chdir(homeDir+'/submissions')
student = raw_input("login name?")
pathname = ""
studentsrc = []
#print  student
targetFiles = []
targetFiles.append("Chamber.java")
targetFiles.append("Passage.java")
targetFiles.append("Door.java")
targetFiles.append("PassageSection.java")
targetFiles.append("Space.java")
studentDir = student+'/a2/src'

pathname = os.path.abspath(studentDir);
#print pathname
if (os.path.isdir(pathname)):
	os.chdir(pathname)
	files = getfiles(".")
	for f in files:
		pos = f.rfind('/')
		filename = f[pos+1:]
		if filename in targetFiles:
			shutil.copy(f, homeDir+"/src/"+filename)
os.chdir(homeDir+"/src")
for f in os.listdir('.'):
	removePackage(f);
os.chdir(homeDir)
p = Popen( ["ant","-f" , "junit.xml" ],stdout=subprocess.PIPE, stderr=subprocess.STDOUT) #,shell=True,stderror=subprocess.STDOUT,)
good, text = p.communicate()
print(good)
for line in good.split("\n"):
	if "passed:" in line:
		pos = line.rfind(":")
		num = line[pos+1:]
		print student + ","+ num
#print(text)

#p = Popen(["rm", "src/*" ],stdout=subprocess.PIPE, stderr=subprocess.STDOUT) #,shell=True,stderror=subprocess.STDOUT,)
#good, text = p.communicate()
os.chdir(homeDir)

		
