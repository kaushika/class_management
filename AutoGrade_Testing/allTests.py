#!/usr/bin/python
# 
import  csv,sys, os, re, pexpect, getpass
from pexpect import pxssh
import getpass
import random

def cleanup(username, hostname, password):
	s = pxssh.pxssh()
	try:
		s.login(hostname, username, password)
		s.sendline('rm -rf ~/Desktop/test')   # run a command
		s.prompt()             # match the prompt
		print(s.before)        # print everything before the prompt.
		s.sendline('mkdir ~/Desktop/test')   # run a command
		s.prompt()             # match the prompt
		print(s.before) 
		s.logout()
	except pxssh.ExceptionPxssh as e:
		print("pxssh failed on login.")
		print(e)


def pushExam(test, username, password):
	command = 'scp -r test/'+test+" "+ username+'@linux.socs.uoguelph.ca:Desktop/test/'
	expected = username+"@linux.socs.uoguelph.ca's password:"
	child = pexpect.spawn(command)
	child.expect(expected)
	child.sendline(password)
	child.expect(pexpect.EOF, timeout=None)

def selectExam():
	testNum = random.randint(1,30)
	
	return "t" + str(testNum)

hostname = "linux.socs.uoguelph.ca"
password = getpass.getpass("password for exam acounts: ")

accounts = []
for i in range(0,16):
	accounts.append("cis2430_"+str(i))

choice = 'y'

while choice == 'y':
	for acc in accounts:
		print("working on "+ acc)
		cleanup(acc, hostname ,password)
		pushExam(selectExam(),acc,password)
	
	choice = raw_input("Setup another set? y/n ")
