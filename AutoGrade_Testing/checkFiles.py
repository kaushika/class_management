import os, sys
from subprocess import call
from subprocess import check_output


assign = raw_input("Which student repo set would you like to check for files? (labs, A1, A2, A3, A4): ")
home = check_output("pwd")
home = home.rstrip('\n')
homeDir = home +'/'+ assign
logFl = raw_input("Name of output file? ")
logger = open(logFl,'w')
os.chdir(homeDir)
for student in os.listdir('.'):
	studentDir = student+'/'+assign
	if (os.path.isdir(studentDir)):
		os.chdir(studentDir)
		files = os.listdir('.')
		if '.git' in files:
			if len(files) > 3:
				print (student)
				logger.write(student)
				logger.write("\n")
	os.chdir(homeDir)
logger.close()