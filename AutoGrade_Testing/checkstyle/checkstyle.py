import  csv,sys, os, re, shutil
import subprocess
from subprocess import call
from subprocess import check_output
from subprocess import Popen

def processStudent(student, basedir):
	numErrors = 0
	output = []
	#print ("HERE I AM" + basedir)
	base = '-Dbasedir='+basedir
	command=[]
	command.append('ant')
	command.append(base)
	command.append('-f')
	command.append('check.xml')
	#print " ".join(command)
	try:
		c = Popen(command,stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
		output, errors = c.communicate()
		numErrors = output.count('[ERROR]')
		#print numErrors
		#print(errors)
	except Exception as inst:
		print type(inst)
		print inst
	#print numErrors
	categories = [];
	lines = output.split('\n')
	for line in lines:
		bits = line.split("[")
		#print line
		#print(bits[-1])
		if(len(bits)>2):
			#print(bits[2])		
			if ("ERROR]" in bits[2]):
				categories.append(re.sub(']',"",bits[-1]))

	categories = list(dict.fromkeys(categories))
	#print(" ".join(categories))
	print(student + "," + str(len(categories)))




assignDir= "/submissions"
homeDir = check_output("pwd")
homeDir = homeDir.rstrip('\n')
workingDir = homeDir+assignDir
#os.chdir(homeDir+assignDir)
for student in os.listdir(workingDir):
	#print "working on " + student
	studentDir = workingDir+"/"+student+'/a1'
	#print studentDir
	if (os.path.isdir(studentDir)):
		processStudent(student,studentDir)
	
