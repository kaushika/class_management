#!/usr/bin/python
import  csv,sys, os, re, shutil
import subprocess
from subprocess import call
from subprocess import check_output
from subprocess import Popen
import tarfile
import zipfile
import rarfile
import signal
import ConfigParser
import threading
from Queue import Queue
#-------------------------------------
# class for running separate processes with an expiration timer
#https://stackoverflow.com/questions/1191374/using-module-subprocess-with-timeout
#-----------------------------------------

class Command(object):
    def __init__(self, cmd):
        self.cmd = cmd
        self.process = None


    def run(self, timeout):

        que = Queue()
        def target(out):

            #bad = []
            #print 'Thread started at: ' + check_output("pwd") + "with this command: " + " ".join(self.cmd)
            try:
                self.process = subprocess.Popen(self.cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=False)
                good, bad  = self.process.communicate()
                #print "Right after process is done communicating**********"
                out.put(good)
            except Exception as e:
                print "Exception in target method: " 
                print e
                return

            #print 'Thread finished'
            #return out, bad

            
        thread = threading.Thread(target=target, args = [que])
        thread.start()         
        thread.join(timeout)
        if thread.is_alive():
            print 'Terminating process'
            self.process.terminate()
            thread.join()
        results =que.get() 
        #print "Right after results getting from queue**********"
        #print results        
        #print self.process.returncode
        return results

#http://stackoverflow.com/questions/898669/how-can-i-detect-if-a-file-is-binary-non-text-in-python
def is_binary(filename):
    """Return true if the given filename is binary.
    @raise EnvironmentError: if the file does not exist or cannot be accessed.
    @attention: found @ http://bytes.com/topic/python/answers/21222-determine-file-type-binary-text on 6/08/2010
    @author: Trent Mick <TrentM@ActiveState.com>
    @author: Jorge Orpinel <jorge@orpinel.com>"""
    fin = open(filename, 'rb')
    try:
        CHUNKSIZE = 1024
        while 1:
            chunk = fin.read(CHUNKSIZE)
            if '\0' in chunk: # found null byte
                return True
            if len(chunk) < CHUNKSIZE:
                break # done
    # A-wooo! Mira, python no necesita el "except:". Achis... Que listo es.
    finally:
        fin.close()

    return False
#---------------
# recursively get a list of all the files given a path
#----------------
def getfiles(path):
    if os.path.isdir(path):
        for root, dirs, files in os.walk(path):
            if ".git" in dirs:
                dirs.remove(".git")
            if ".svn" in dirs:
                dirs.remove(".svn")
            if ".empty" in files:
                files.remove(".empty")
            for name in files:
                yield os.path.join(root, name)
    else:
        yield path


def unpackFiles(config):
    #get the current directory
    success = 1
    homeDir = check_output("pwd")
    homeDir = homeDir.rstrip('\n')
    #get the file and folder names
    aName = config.get("Musts", "Assign")
    subFile = config.get("Musts","StudentZip")
    testFile = config.get("Musts", "TestZip")
    #set up assignment directory
    if (os.path.isdir("subs")):
        shutil.rmtree("subs") #get rid of any existing files
    call(['mkdir',"subs"]) #put the subs directory back
    assignDir = homeDir + "/subs"

    if (os.path.isdir("raw")):
        shutil.rmtree("raw") #get rid of any existing files
    call(['mkdir',"raw"]) #put the subs directory back
    zipDir =  homeDir + "/raw"

    p = Popen(['unzip', '-d', assignDir, subFile],stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    good, output = p.communicate()
 
    #call(['unzip', '-d', assignDir, subFile])
    files = os.listdir(assignDir)
    if '.DS_Store' in files:
        files.remove('.DS_Store')
    if '__MACOSX' in files:
        files.remove('__MACOSX')
    if 'index.html' in files:
        files.remove('index.html')
    if len(files) is 1:
        assignDir = assignDir + "/" + str(files[0])


    #unzip and rename student directories,
    #this code assumes that the student submission is compressed in someway
    os.chdir(assignDir)
    for packfile in os.listdir(assignDir):
        filename = packfile.replace(",","")
        filename = filename.replace("(","")
        filename = filename.replace(")","")
        filename = filename.replace("'","")
        names = filename.split(' - ') #courselink specific
        #names = parts[0].split(' ')
        #print names
        if ('.DS' in names) or ('index.html' in names):
            continue
    #TODO add the code that pulls out the usernames from the text file
        # courselink line
        studentName = names[len(names)-3]
        studentName = studentName.replace(" ","")
       
        # moodle line
        #studentName = names[len(names)-1]+names[0]
        #print studentName
        submitted = studentName+'/' + names[len(names)-1]
        #print submitted
        if not (os.path.isdir(studentName)):
            call(['mkdir', studentName])
        filename = filename.replace(" ","")
        call(["mv", packfile, submitted])

    #unzip the student code

    studentlist = os.listdir(assignDir)
    if '.DS_Store' in studentlist:
        studentlist.remove('.DS_Store')
    if 'index.html' in studentlist:
        studentlist.remove('index.html')
    errorList = ""
    errorcount = 0
    for student in studentlist: 

        os.chdir(student)
        files = os.listdir('.')
        if '.DS_Store' in files:
            files.remove('.DS_Store')
        if '__MACOSX' in files:
            files.remove('__MACOSX')
        if len(files) is 1:
            file = files[0]
            if file.endswith('.zip'):
                opener, mode = zipfile.ZipFile, 'r'
            elif file.endswith('.tar.gz') or file.endswith('.tgz'):
                opener, mode = tarfile.open, 'r'
            elif file.endswith('.tar.bz2') or file.endswith('.tbz') or file.endswith('bz2'):
                opener, mode = tarfile.open, 'r:bz2'
            elif file.endswith('.tar'):
                opener, mode = tarfile.open, 'r'
            elif file.endswith('.gz') or file.endswith('.c.gz'):
                opener, mode = tarfile.open, 'r:gz' 
            elif file.endswith('.rar'):
                opener, mode = rarfile.RarFile, 'r'
    #       else: 
    #           print errorList
                errorList = errorList + student + '\n'
            try:

                output = opener(file, mode)
                #fnames = output.namelist()
                output.extractall()
                output.close
                shutil.move(file,zipDir)

            except:
                #print errorList
                errorList = errorList + student + '\n'
                errorcount = errorcount+1
                #call(["mv", home+'/tarballs/'+ file, '.'])

        unzipped = os.listdir('.')
        if '.DS_Store' in unzipped:
            shutil.rmtree('.DS_Store')
        if '__MACOSX' in unzipped:
            shutil.rmtree('__MACOSX')

        for f in getfiles('.'):
            if os.path.isfile(f):
                if f.endswith('.c'):
                    if(is_binary(f)):
                        #print student + " " + f + " is a binary file \n"
                        call(["mv", f, f + ".binary"])


        os.chdir(assignDir)

    #print "Number of students whose submissions were not unzipped:" +  str(errorcount) +  "\n"
    #print errorList    

    errorList = ""
    #change name of submission folder to assignment name
    for student in os.listdir(assignDir):
        if not os.path.isdir(student):
            continue #skipping over the index.html files that courselink insists on inserting
        

        os.chdir(student)



        studentSub = os.listdir('.')
        if len(studentSub) != 1:  #don't even try to recover from weird submissions
            if not os.path.isdir(aName):
                os.mkdir(aName)
            for file in studentSub:
                shutil.move(file, assignDir + '/' + student + '/'+aName)
            errorList = errorList + "submission in improper format, "
            #success = 0
            #os.chdir(assignDir)s
            #continue
        else:
            shutil.move(studentSub[0], assignDir + '/' + student + '/'+aName)
		

#copy testing code to student directory
        
        zp = Popen(['unzip', '-d', '.', homeDir+'/'+testFile],stdout=subprocess.PIPE, stderr=subprocess.STDOUT )
        #call(['unzip', '-d', '.', homeDir+'/'+testFile])
        out, err = zp.communicate()
        testDir = config.get("Musts", "TestDir")
        if os.path.isdir(aName+"/include/"):
            os.chdir(aName+"/include/")
#        else:
#            os.chdir(aName)
        for header in os.listdir("."):
            if header != "LinkedListAPI.h":
                if(header.endswith('h')) and not os.path.isdir(header):
                    shutil.copyfile(header,assignDir + "/"+ student + "/" + testDir+"/include/"+header)
        os.chdir(assignDir + "/"+student)
        if os.path.isdir(aName+"/src/"):
            os.chdir(aName+"/src/")
        else:
            if(os.path.isdir(aName)):
               os.chdir(aName)
            else:
               continue

        for source in os.listdir("."):
            if source.endswith('c'):
                if not "main" in open(source).read() and not os.path.isdir(source):
                    shutil.copyfile(source,assignDir + "/"+ student + "/" + testDir+"/studentCode/"+source)
            if os.path.isdir(source):  #one level of subdirectories supported
                os.chdir(source)
                for morefiles in os.listdir("."):
                    if morefiles.endswith('c'):
                        if not "main" in open(morefiles).read() and not os.path.isdir(morefiles):
                            shutil.copyfile(morefiles,assignDir + "/"+ student + "/" + testDir+"/studentCode/"+morefiles)

        os.chdir(assignDir)
    #print "These students did not have directories that could be manually processed."
    #print errorList;
    return errorList



######################
#program starts here
#######################
errorWarningList = []
scores = []
scores.append("username,score, cppcheck, compile, testRun, apiTest")
print "remember to remove the file from hashTest.c"
unPackErrors = []
homeDir = check_output("pwd")
homeDir = homeDir.rstrip('\n')
config = ConfigParser.ConfigParser()
config.readfp(open('tests.cfg'))
unpack = config.getboolean('Musts', 'Unzip')
testDir = config.get('Musts', 'TestDir')
assignDir = homeDir + "/subs"
if(unpack):
    unPackErrors = unpackFiles(config)  #unpacks everything and moves it into place

os.chdir(assignDir)
aName = config.get("Musts", "Assign")
count = 2
for student in os.listdir('.'):
    errorString = ""
    if(student == ".DS_Store") or (student == "index.html"):
        continue #silly macs
 #   print "*****************************"
    print student
 #   print "*****************************"
    studentScore =student + ",=f"+str(count)+"/9*4+e"+str(count)+" -(c"+str(count)+"/5 +d"+str(count)+"/5)"
    count = count + 1
    if (os.path.isdir(student + '/' + aName)):
        os.chdir(student + '/' + aName)
# run cpp check
        #print "---cppcheck---"
        numErrors = 0
        try:
            #good = call(['cppcheck', '--enable=all', '--language=c', '--std=c99', '--inconclusive', '--suppress=missingInclude', 'src/*.c', '-i', './include'])
            c = Popen(['cppcheck', '--enable=all', '--language=c', '--std=c99', '--inconclusive', '--suppress=missingInclude', 'src', '-i', './include'],stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            #c = Popen(['cppcheck', 'src', '-i', './include'],stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

            good, output = c.communicate()
            #print good
            numErrors = good.count('error')
            numMem = good.count('Memory')
            numErrors = numErrors - numMem
        except Exception as inst:
            print type(inst)
            print inst
        studentScore = studentScore+ ","+str(numErrors)


#compile the students test code
        #print "---compiling test program---"
        try:
            p = Popen(['make'],stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            good, output = p.communicate()
            errorcount = good.count("error")
            warningcount = good.count("warning")
            # if(errorcount >0) or (warningcount > 0):
            #     errorString = errorString + "errors/warnings on make, "
            #     #errorWarningList.append(student + ": Test program compiled with errors/warnings")
            studentScore = studentScore + ','+str(errorcount+warningcount)
        except Exception as inst:
            errorString = errorString + "student make failed, "
            #errorWarningList.append(student + ": Test program did not compile")
            studentScore = studentScore + ',100'


    else:
        errorString = errorString + "could not find lab directory, "
        #errorWarningList.append(student + ": Directory structure issues")
        studentScore = studentScore + ",0,0"

#attempt to run the students' test code
    #print "---running test program---
    
    #print check_output("pwd")
    runnables = ""
    if (os.path.isdir("bin")):
        os.chdir("bin")        
        runnables = os.listdir(".")
        if '.DS_Store' in runnables:
            runnables.remove('.DS_Store')
        if '__MACOSX' in runnables:
            runnables.remove('__MACOSX')

        score = 0;
        for runMe in runnables:
            if not os.path.isdir(runMe):
                if is_binary(runMe):
                    #print "running " + runMe + " for " + student
                    try:
                        command = Command(["./" + runMe])
                        #command = Command(["ls"])
                        good = command.run(timeout=5)
                        #r = Popen(["./" + runMe],stdout=subprocess.PIPE, stderr=subprocess.STDOUT )
                        #good, error = r.communicate()
                        #good = check_output( ["./" + runMe], stderr=STDOUT, timeout=15)
                        #print good

                        numErrors = good.count("error");
                        if("Terminating" in good):
                            errorString = errorString + "Infinite loop or menu, "
                            score = 0
                        else:
                            numLines = good.count("\n")
                            if(numLines > 15):
                                score = 1
                            else:
                                score = 0
                    except Exception as inst:
                        #print "exception running student test program" + " ".join(inst.args)
                        #print inst
                        errorString = errorString + "Testing did not run, "
                        #errorWarningList.append(student + ": Test Program did not run")
        studentScore = studentScore + ','+str(score)
    else:
#        errorWarningList.append(student + ":no bin directory could not run test program")
        errorString = errorString + "no bin directory, "
        studentScore = studentScore + ",0"              

     #run the test harness   
    #print "---running API test suite---"    
    os.chdir(assignDir)
    if(os.path.isdir(student + "/" + testDir)):
        os.chdir(student + "/" + testDir)
        try:
            ts = Popen(["make"], stdout=subprocess.PIPE, stderr=subprocess.STDOUT )
            out, err = ts.communicate()
            if("error" in out):
                errorString = errorString + "problems compiling API suite, "
                score = 0
            else:    
                command = Command(['./bin/tests'])
                theResults = command.run(timeout=5)        
                #run = Popen(['./bin/tests'],stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
                #theResults,err = run.communicate()
                #theResults = check_output(['./bin/tests'])
                outputfile = open(student+"results.txt", 'w')
                outputfile.write(theResults)
                outputfile.close()
                if("Terminate" in theResults):
                    score = 0;
                    errorString = errorString + "infinite loop in API suite, "
                else:
                    score = theResults[-2]
                studentScore = studentScore + "," + score

        except Exception as inst:
            errorString = errorString + "exception running API suite, "
            #errorWarningList.append(student + ": Rerun API Suite-probably due to seg fault")
            studentScore = studentScore + ",0" 
            #print "--problems with API test suite"
    else:
        errorString = errorString + "no tests directory, "
 

    scores.append(studentScore)
    if (errorString != ""):
        errorWarningList.append(student + ": "+ errorString)  
    os.chdir(assignDir)





os.chdir(homeDir)
#write to files
err=open('errorCases.txt', 'w')
if(unpack):
    err.write("Directory structure could not be automatically processed:")
    err.write(unPackErrors)
err.write("\nplease manually grade the following students\n")
err.write("\n".join(errorWarningList))
res = open('results.csv', 'w')
res.write("\n".join(scores))


#write to screen
if(unpack):
    print "\nProblems unpacking for: "
    print unPackErrors
print "\nManually Grade:"

print "\n".join(errorWarningList)
print "\n"


