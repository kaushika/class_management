#!/usr/bin/python
# 
import gitFunctions
import  csv,sys, os, re, pexpect, getpass
import subprocess
from subprocess import call
from subprocess import check_output
import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText

msgMid = ""
msgEnd = "\nPlease let me know by return email if there is a reason that your assignment has not been submitted.  If you have dropped the course, please tell me as the University does not notify me when someone drops the class."	
msgEnd = msgEnd + "\n Judi\n\n"



server = smtplib.SMTP("mail.uoguelph.ca")
FROM = "cis2500@socs.uoguelph.ca"

rootdir = check_output("pwd")
rootdir = rootdir.rstrip('\n')

while True:
	email = raw_input("Do you want to email students with their assignment status? (Y/n)")
	if email == "Y":
		break
	elif email == "n":
		break


assign = raw_input("Which Assignment to check?" )

#Pull repositories to ensure they are up to date
gitFunctions.pullRepos ()
os.chdir(rootdir)



msgStart = "Hi, \nYou are getting this email to let you know your submission state for " + assign + "\n\n"

iiiiii c(stFile,delimiter=',')
                wr.writerow([student,status,assign])eck_output "pwd")
home = home.rstrip('\n')
homeDir = home + "/studentWork" 
os.chdir(homeDir)

status = ""

listFile = open("../student_list.csv",'wb')
header = False

wr = csv.writer(listFile,delimiter=',')
wr.writerow(["Student ID","Run Status - 00 = Ran Successfully: 01 = No Assignment Folder: 02 = No Files: 03 = Missing Makefile: 04 = Doesn't Compile","Assignment"])

for student in os.listdir('.'):
	if(student != ".DS_Store"):
#		print student
		studentDir = student+'/coursework/'+assign
		if (os.path.isdir(studentDir)):

			files = os.listdir(studentDir)
			if '.git' in files:
				files.remove('.git')

			if files == [ ] :
				msgMid = msgMid + "Your submission does not have any files\n"
				status = "02"

			else :
				try:
					os.chdir(studentDir)
					result = check_output('make') #,shell=True,stderror=subprocess.STDOUT,)

					msgMid = assign + " compiles successfully. The output of Make is: "
					msgMid = msgMid + result + "\n"
					status = "00"

				except:
					#print "Make exception"
					
					if 'Makefile' or 'makefile' in files :
						msgMid = msgMid + assign + " submission exists but doesn't compile\n"
						status = "04"

					else :
						msgMid = msgMid + "Makefile missing\n"
						status = "03"

		else :

			msgMid = msgMid + assign + " directory not found\n"
			status = "01"


		print student
		#TO = student + "@uoguelph.ca"
		TO = "judi@uoguelph.ca"

		msg = msgStart + msgMid + msgEnd

		print status

		if email == "Y" :
			#Send the student a message containing the status of their submission
			message = MIMEMultipart()
 	 		message['From'] = FROM
	 		message['Subject'] = "Your " + assign + " Submission"
	 		message['To']= TO
	 		message.attach(MIMEText(msg, 'plain'))
	 		server.sendmail(FROM, TO, message.as_string())



		wr = csv.writer(listFile,delimiter=',')
		wr.writerow([student,status,assign])



		os.chdir(homeDir)
		msgMid = ""
		msg = ""

listFile.close()
