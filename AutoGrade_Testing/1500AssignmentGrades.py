#!/usr/bin/python
# use pathname to csv files + Y for DE or N for not DE grades
import csv, sys, os, re
de = 0


f = open("slackers-1500.txt",'r')
slackers = f.readlines()
#print len(slackers)
assignment = sys.argv[1]
noGrade = open(assignment+"zeros.csv", 'w')
de = open(assignment+"DE.csv", 'w')
f2f = open(assignment+".csv", 'w')
path = "marking/%s" % assignment
de.write( "LastName,FirstName,LoginID,total,comment\n")
f2f.write( "LastName,FirstName,LoginID,total,comment\n")
for file in os.listdir(path):
	filePath = path+"/"+file
	if file.endswith(".csv"):
#		print "Working on file ",file
		reader = csv.reader (open (filePath, "rU"))
		header = next (reader, None) #skip header
		next (reader, None) #skip header
		for student in reader:
#			print "working on: ", student
			if student[1] != "":
				if student[6]!="0" and student[6]!="":
	#				print "finding marks"
					marks = "Programming Style: %s Output: %s Following Spec: %s Input: %s Indentation and Variables: %s If or Switch use: %s " %(  student[7], student[8], student[9], student[10], student[11], student[12])			
	#				print "extracting comment"
					comment = marks + student[13].replace(",","")				
	#				print comment
					row = "%s,%s,%s,%s,%s\n" % (student[1],student[2],student[4],student[6],comment)
				else:	
					if student[13] != "":
						comment = student[13].replace(",","")
					else:
						comment = "No record of submission"
					row = "%s,%s,%s,%s,%s\n" % (student[1],student[2],student[4],0,comment)
					noGrade.write(row)	
				for slacker in slackers:
					slacker = slacker.strip()
					if slacker == student[4]:
	#					print slacker + " " + student[4]
						row = "%s,%s,%s,%s,%s\n" % (student[1],student[2],student[4],"","Grade Witheld Academic Integrity Unit incomplete")
				if student[3] =="DE":
					de.write(row)
				else:
					f2f.write(row)

			


f.close()
de.close()
f2f.close()
noGrade.close()
	
		
#student[13].replace(",","").replace("\\n","")		